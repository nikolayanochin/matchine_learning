<?php
/**
 * Created with love by АльянсЭкспресс.
 * Author: Anochin Nikolay
 * Email: titan12345@mail.ru
 * Phone: +7 925 174 1314
 * Date: 13.11.2018
 * Time: 22:40
 */


namespace lib;

/**
 * Class NeuralNetwork
 * @package lib
 */
class NeuralNetwork
{
    const TYPE_INPUT = 1;
    const TYPE_HIDDEN = 2;
    const TYPE_OUTPUT = 3;

    /** @var array */
    private $layers = [];


    private $result_values = [
        0 => [1, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        1 => [0, 1, 0, 0, 0, 0, 0, 0, 0, 0],
        2 => [0, 0, 1, 0, 0, 0, 0, 0, 0, 0],
        3 => [0, 0, 0, 1, 0, 0, 0, 0, 0, 0],
        4 => [0, 0, 0, 0, 1, 0, 0, 0, 0, 0],
        5 => [0, 0, 0, 0, 0, 1, 0, 0, 0, 0],
        6 => [0, 0, 0, 0, 0, 0, 1, 0, 0, 0],
        7 => [0, 0, 0, 0, 0, 0, 0, 1, 0, 0],
        8 => [0, 0, 0, 0, 0, 0, 0, 0, 1, 0],
        9 => [0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
    ];
    private $result_value;


    public function addNewLay($count_neurons, $type)
    {
        $neurons = [];
        $input_values = [];

        if ($type !== self::TYPE_INPUT) {
            $last_count_neurons = count($this->layers[count($this->layers) - 1]['neurons']);

            /** @var Neuron $neuron */
            foreach ($this->layers[count($this->layers) - 1]['neurons'] as $neuron) {
                $input_values[] = $neuron->calcA();
            }
        }

        while ($count_neurons--) {
            $neuron = new Neuron($type);
            if (isset($last_count_neurons)) {
                $neuron->setRandWeights($last_count_neurons);
                $neuron->setInputValues($input_values);
            }
            $neurons[] = $neuron;
        }

        $this->layers[] = ['type' => $type, 'neurons' => $neurons];
    }


    public function setData($data, $result)
    {
        /** @var Neuron $neuron */
        foreach ($this->layers[0]['neurons'] as $index => $neuron) {
            $neuron->input_values[0] = $data[$index];
        }

        if (count($this->layers) === 3) {
            foreach ($this->layers[count($this->layers) - 2]['neurons'] as $neuron) {
                $neuron->setInputValues($data);
            }
        }

        if ($result !== false) {
            $this->result_value = $result;
        }
    }


    public function learning()
    {
        $layers_count = count($this->layers);

        $input_values = [];
        /** @var Neuron $neuron */
        foreach ($this->layers[count($this->layers) - 2]['neurons'] as $neuron) {
            $input_values[] = $neuron->calcA();
        }


        while ($layers_count-- > 1) {
            $sum = 0;
            /** @var Neuron $neuron */
            foreach ($this->layers[$layers_count]['neurons'] as $i => $neuron) {
                $wait = 0;
                if ($neuron->type === NeuralNetwork::TYPE_OUTPUT) {
                    $wait = $this->result_values[$this->result_value][$i];
                }

                if ($neuron->type === NeuralNetwork::TYPE_OUTPUT) {
                    $neuron->setInputValues($input_values);
                }

                if ($neuron->type === NeuralNetwork::TYPE_OUTPUT) {
                    $neuron->calcError($wait);
                } else {
                    $neuron->calcError($wait, $i, $this->layers[$layers_count + 1]['neurons']);
                }

                $neuron->changeWeight();

                if ($layers_count == 2) {
                    $diff = ($this->result_values[$this->result_value][$i] - $neuron->calcA());
                    $sum += pow($diff, 2);
                }

            }
            if ($layers_count == 2) {
                $total_error = $sum * 0.5;
            }
        }

        return $total_error;
    }


    public function getResult()
    {
        $input_values_1 = [];
        /** @var Neuron $neuron */
        foreach ($this->layers[0]['neurons'] as $neuron) {
            $input_values_1[] = $neuron->calcA();
        }

        $input_values_2 = [];
        /** @var Neuron $neuron */
        foreach ($this->layers[count($this->layers) - 2]['neurons'] as $neuron) {
            $neuron->setInputValues($input_values_1);
            $input_values_2[] = $neuron->calcA();
        }

        $result = 0;

        /** @var Neuron $neuron */
        foreach ($this->layers[count($this->layers) - 1]['neurons'] as $neuron) {
            $neuron->setInputValues($input_values_2);
            $result += $neuron->calcA();
        }

        $out = [];
        /** @var Neuron $neuron */
        foreach ($this->layers[count($this->layers) - 1]['neurons'] as $neuron) {
            $out[] = $neuron->calcA() / $result * 100;
        }

        return $out;
    }

}