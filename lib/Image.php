<?php
/**
 * Created with love by АльянсЭкспресс.
 * Author: Anochin Nikolay
 * Email: titan12345@mail.ru
 * Phone: +7 925 174 1314
 * Date: 13.11.2018
 * Time: 21:10
 */


namespace lib;

/**
 * Class Image
 * @package lib
 */
class Image
{
    private $path;
    private $resource;
    private $image_create_func;
    private $image_save_func;
    private $new_image_ext;
    private $bin_array = [];

    public function __construct($path)
    {
        $this->path = $path;
        $info = getimagesize($this->path);
        $mime = $info['mime'];
        switch ($mime) {
            case 'image/jpeg':
                $this->image_create_func = 'imagecreatefromjpeg';
                $this->image_save_func = 'imagejpeg';
                $this->new_image_ext = 'jpg';
                break;
            case 'image/png':
                $this->image_create_func = 'imagecreatefrompng';
                $this->image_save_func = 'imagepng';
                $this->new_image_ext = 'png';
                break;
            case 'image/gif':
                $this->image_create_func = 'imagecreatefromgif';
                $this->image_save_func = 'imagegif';
                $this->new_image_ext = 'gif';
                break;
            default:
                throw new \Exception('Unknown image type.');
        }

        $this->resource = call_user_func($this->image_create_func, $this->path);
    }

    public function cutImage()
    {
        //load the image
        $img = call_user_func($this->image_create_func, $this->path);

        //find the size of the borders
        $b_top = 0;
        $b_btm = 0;
        $b_lft = 0;
        $b_rt = 0;

        //top
        for (; $b_top < imagesy($img); ++$b_top) {
            for ($x = 0; $x < imagesx($img); ++$x) {
                if (imagecolorat($img, $x, $b_top) != 0xFFFFFF) {
                    break 2; //out of the 'top' loop
                }
            }
        }

        //bottom
        for (; $b_btm < imagesy($img); ++$b_btm) {
            for ($x = 0; $x < imagesx($img); ++$x) {
                if (imagecolorat($img, $x, imagesy($img) - $b_btm - 1) != 0xFFFFFF) {
                    break 2; //out of the 'bottom' loop
                }
            }
        }

        //left
        for (; $b_lft < imagesx($img); ++$b_lft) {
            for ($y = 0; $y < imagesy($img); ++$y) {
                if (imagecolorat($img, $b_lft, $y) != 0xFFFFFF) {
                    break 2; //out of the 'left' loop
                }
            }
        }

        //right
        for (; $b_rt < imagesx($img); ++$b_rt) {
            for ($y = 0; $y < imagesy($img); ++$y) {
                if (imagecolorat($img, imagesx($img) - $b_rt - 1, $y) != 0xFFFFFF) {
                    break 2; //out of the 'right' loop
                }
            }
        }

        $new_img = imagecreatetruecolor(
            imagesx($img) - ($b_lft + $b_rt), imagesy($img) - ($b_top + $b_btm));
        imagecopy($new_img, $img, 0, 0, $b_lft, $b_top, imagesx($new_img), imagesy($new_img));

        $this->resource = $new_img;
    }

    public function resizeImage($w, $h)
    {
        $width = imagesx($this->resource);
        $height = imagesy($this->resource);
        $r = 1;

        if ($w / $h > $r) {
            $new_width = $h * $r;
            $new_height = $h;
        } else {
            $new_height = $w / $r;
            $new_width = $w;
        }

        $src = call_user_func($this->image_create_func, $this->path);
        $dst = imagecreatetruecolor($new_width, $new_height);
        imagecopyresampled($dst, $src, 0, 0, 0, 0, $new_width, $new_height, $width, $height);

        $this->resource = $dst;
    }

    public function saveImage()
    {
        call_user_func($this->image_save_func, $this->resource, $this->path);
    }


    public function imageToPixelArray()
    {
        $width = imagesx($this->resource);
        $height = imagesy($this->resource);


        for ($y = 0; $y < $height; $y++) {
            for ($x = 0; $x < $width; $x++) {
                // pixel color at (x, y)
                $color = imagecolorat($this->resource, $x, $y);
                $r = ($color >> 16) & 0xFF;
                if ($r > 225) {
                    $this->bin_array[] = 0;
                } else {
                    $this->bin_array[] = 1;
                }
            }
        }
        return $this->bin_array;
    }

    public function printBinary()
    {
        foreach ($this->bin_array as $index => $item) {

            if ($item === 1) {
                echo "\e[42;30m" . $item . "\e[0m ";
            } else {
                echo $item . " ";
            }

            if (($index + 1) % 10 === 0) {
                echo "\n";
            }
        }
    }
}