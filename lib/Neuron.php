<?php
/**
 * Created with love by АльянсЭкспресс.
 * Author: Anochin Nikolay
 * Email: titan12345@mail.ru
 * Phone: +7 925 174 1314
 * Date: 13.11.2018
 * Time: 22:37
 */


namespace lib;

/**
 * Class Neuron
 * @package lib
 */
class Neuron
{
    public $input_values = [];
    public $type;
    public $input_weights = [];
    public $error = null;
    private $calcK = null;
    private $calcA = null;

    public function __construct($type)
    {
        $this->type = $type;
    }


    public function setRandWeights($last_count_neurons)
    {
        while ($last_count_neurons--) {
            $this->input_weights[$last_count_neurons] = $this->random_float(-1, 1);
        }
    }

    function random_float($min, $max)
    {
        return ($min + lcg_value() * (abs($max - $min)));
    }

    public function setInputValues($input_values)
    {
        $this->input_values = $input_values;
    }

    public function calcError($wait = 0, $index = 0, array $n_out = null)
    {
        $out = $this->calcA();
        if ($this->type === NeuralNetwork::TYPE_OUTPUT) {
            $this->error = $out * (1 - $out) * ($wait - $out);
        } else {
            $sum = 0;
            foreach ($n_out as $neuron) {
                $sum += $neuron->input_weights[$index] * $neuron->error;
            }
            $this->error = $out * (1 - $out) * $sum;
        }
    }

    public function calcA()
    {
        if ($this->type === NeuralNetwork::TYPE_INPUT) {
            return $this->input_values[0];
        }

        $this->calcA = 1 / (1 + exp($this->calcK() * (-1)));

        return $this->calcA;
    }

    public function calcK()
    {
        $sum = 0;
        foreach ($this->input_values as $i => $input_value) {
            $sum += $input_value * $this->input_weights[$i];
        }
        $this->calcK = $sum;
        
        return $this->calcK;
    }

    public function changeWeight()
    {
        $alpha = 0.5;
        foreach ($this->input_weights as $i => $weight) {
            $this->input_weights[$i] = $weight + $alpha * $this->error * $this->input_values[$i];
        }
    }
}