<?php
/**
 * Created with love by АльянсЭкспресс.
 * Author: Anochin Nikolay
 * Email: titan12345@mail.ru
 * Phone: +7 925 174 1314
 * Date: 11.10.2018
 * Time: 16:34
 */
ini_set('memory_limit', '3024M');
require_once('autoload.php');

use lib\Image;
use lib\NeuralNetwork;

$path = __DIR__ . '/network.txt';
$content = file_exists($path) ? file_get_contents($path) : false;
$network_file = unserialize($content);

if (!$content) {
    $ages = 100;
    while ($ages--) {
        $num = 10;
        while ($num-- > 0) {
            $n = 7;
            $image = new Image(__DIR__ . '/docks/' . $num . '.png');
            $image->cutImage();
            $image->saveImage();
            $image->resizeImage(10, 10);
            $bin_array = $image->imageToPixelArray();
            $image->saveImage();

            if (!isset($network)) {
                $network = new NeuralNetwork();
                $network->addNewLay(100, NeuralNetwork::TYPE_INPUT);
                $network->setData($bin_array, $num);
                $network->addNewLay(100, NeuralNetwork::TYPE_HIDDEN);
                $network->addNewLay(10, NeuralNetwork::TYPE_OUTPUT);
                while(($a = $network->learning()) > 0.01);
            } else {
                $network->setData($bin_array, $num);
                while($network->learning() > 0.01);
                $image->printBinary();
                echo $ages;
            }
        }
    }

    $content = serialize($network);
    file_put_contents($path, $content);
} else {
    $network = $network_file;
}

$image = new Image(__DIR__ . '/input/image.png');
$image->cutImage();
$image->saveImage();
$image->resizeImage(10, 10);
$bin_array = $image->imageToPixelArray();
$image->printBinary();
$image->saveImage();

$network->setData($bin_array, false);


print_r($network->getResult());
